#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>

void sigterm_handler(int i) {
	printf ("exit(sigterm)\n");
	exit(EXIT_SUCCESS);
}

int main(int argc, char const *argv[]){
	struct sigaction sk;
	sk.sa_handler = sigterm_handler;
	
	sigaction(SIGTERM, &sk, 0);
	signal(SIGTSTP, SIG_IGN);

	for(;;){
		sleep(3);
		printf("Workin...\n");
	}
}