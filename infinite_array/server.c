#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <mcheck.h>

#define COMMANDS "stop,info"
#define PORT_UDP_GET 32002
#define PORT_TCP_GET 32000
#define MAXPENDING 10
const char *BROAD;


typedef struct string_array sa;

struct string_array {
	char *string;
	sa *next;
};

sa *HEAD;

void DieWithError(char *errorMessage);
void server_hendler();

void *tcp_order_accept(void *arg);
void *tcp_order_getter(void *arg);
void *udp_order(void *arg);
void control();
int add_array(char *data,int len);
void free_memory_array();
void show_array();




int main(int argc, char const *argv[]) {
	if (argc < 2) {
		fprintf(stderr, "Usage:  %s ip_broadcast\n", argv[0]);
		exit(1);
	}
	printf("Доступные команды: %s\n", COMMANDS);
	mtrace();
	BROAD = argv[1];
	HEAD = malloc(sizeof(sa));

	server_hendler();

	return 0;
}


void server_hendler() {
	int result;
	pthread_t thread;

	if ((result = pthread_create(&thread, NULL, tcp_order_accept, NULL)) != 0) {
		DieWithError("Creating the tcp_order_accept thread");
	}
	if ((result = pthread_create(&thread, NULL, udp_order, NULL)) != 0) {
		DieWithError("Creating the udp_order thread");
	}

	control();
}


void *tcp_order_accept(void *arg) {
	int  servSock, clntSock, result;
	unsigned int lenClntAddr;
	struct sockaddr_in ServAddr, ClntAddr;
	pthread_t thread_client;
	int *clnt_g_args;

	pthread_detach(pthread_self());


	if ((servSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		DieWithError("socket() failed");
	}

	memset(&ServAddr, 0, sizeof(ServAddr));
	ServAddr.sin_family = AF_INET;
	ServAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	ServAddr.sin_port = htons(PORT_TCP_GET);

	if (bind(servSock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0) {
		DieWithError("bind() failed");
	}

	if (listen(servSock, MAXPENDING) < 0) {
		DieWithError("listen() failed");
	}
	for (;;) {

		lenClntAddr = sizeof(ClntAddr);
		if ((clntSock = accept(servSock, (struct sockaddr *) &ClntAddr, &lenClntAddr)) < 0)	{
			DieWithError("accept() failed");
		}

		if ((clnt_g_args = malloc(sizeof(int))) == NULL) {
			DieWithError("malloc() failed");
		}

		*clnt_g_args = clntSock;

		if ((result = pthread_create(&thread_client, NULL, tcp_order_getter, clnt_g_args)) != 0) {
			DieWithError("Creating the first thread_client");
		}

	}

}


void *tcp_order_getter(void *arg) {
	int client_Sock = *(int *) arg;
	free(arg);
	int len;
	char *string;

	pthread_detach(pthread_self());



	if ((recv(client_Sock, &len, sizeof(int), 0)) < 0) {
		DieWithError("recv() failed");
	}
	string = calloc(len, sizeof(char));
	printf("получено сообщение: длина строки %d\n", len );

	if ((recv(client_Sock, string, sizeof(char)*len, 0)) < 0) {
		DieWithError("recv() failed");
	}
	if (add_array(string,len) != 0) {
		DieWithError("add_array() failed");
	}

	printf("получено сообщение: строка: %s\n", string );
	return NULL;

}

void *udp_order(void *arg) {
	int sock_udp;
	struct sockaddr_in addr_udp;
	int udp_str = 1;

	pthread_detach(pthread_self());

	if ((sock_udp = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		DieWithError("socket() failed");
	}
	int status = 1;
	setsockopt(sock_udp, SOL_SOCKET, (SO_BROADCAST | SO_REUSEADDR), &status, sizeof(status));
	memset(&addr_udp, 0, sizeof(addr_udp));
	addr_udp.sin_family = AF_INET;
	addr_udp.sin_addr.s_addr = inet_addr(BROAD);
	addr_udp.sin_port   = htons(PORT_UDP_GET);

	for (;;)
	{
		sleep(3);

		if (sendto(sock_udp, &udp_str, sizeof(int), 0, (struct sockaddr *) &addr_udp, sizeof(addr_udp)) != sizeof(int)) {
			DieWithError("sendto() sent a different number of bytes than expected");
		}

	}


}

void control() {
	char *buf_comand;

	buf_comand = calloc(4, sizeof(char));
	do {
		scanf("%s", buf_comand);
		if (strcmp(buf_comand, "info") == 0) {
			show_array();
		}

		printf("введина команда:%s\n", buf_comand);

	} while ((strcmp(buf_comand, "stop")) != 0);	
	free_memory_array();
	free(buf_comand);
	printf("exit\n");
}

int add_array(char *data,int len) {
	sa *selected=HEAD;
	sa *new;
	if (HEAD->string == NULL) {
		HEAD->string=data;
		printf("stroka zapisanna:%s\n", HEAD->string);
	}else{
		while(selected->next!=NULL){
			selected=selected->next;
		}
		new=malloc(sizeof(sa));
		selected->next=new;
		new->string=data;
	}

	return 0;
}

void free_memory_array() {
	sa *head=HEAD;
	sa *deleted;
	while (head->next != NULL) {
		deleted=head;
		head = head->next;
		free(deleted);
	}
	free(head);
}

void show_array(){
	sa *head=HEAD;
	int i = 1;
	while (head->next != NULL) {

		printf("Stroka №%d:%s\n", i, head->string);
		head = head->next;
		i++;

	}
	// printf("Stroka №%d:%s\n", i, head->string);
}