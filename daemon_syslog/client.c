#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include <syslog.h>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>

const char *IP_ADDR;
#define PORT 55555
#define TIME 600
#define LOG_FILE "./daemon_mysys.log"


int main(int argc, char *argv[]) {
	FILE *pLog;
	int test;
	if( argc!=2 ){
		printf("Usage:  %s ip_address\n", argv[0]);
		exit(1);
	}

	int recv_sock;
	IP_ADDR = argv[1];

	if ((recv_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		printf("socket() failed");
	}

	struct sockaddr_in ServAddr;
	memset(&ServAddr, 0, sizeof(ServAddr));
	ServAddr.sin_family = AF_INET;
	ServAddr.sin_port = htons(PORT);
	ServAddr.sin_addr.s_addr = inet_addr(IP_ADDR);

	if (connect(recv_sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0) {
		printf("connect() failed");
	}
	char *msg;
	msg=calloc(512, sizeof(char)); 
	while((test = recv(recv_sock, msg, (512*(sizeof(char))), 0)) > 0){
	printf("%d\n", test);
    pLog = fopen(LOG_FILE, "a");
    if(pLog == NULL) {
    printf("no open\n");
    }
    // printf("|||||%s", msg);
	fputs(msg, pLog);
	fclose(pLog);
	memset(msg, 0, (512*(sizeof(char))));
	}

    printf("end\n");
	free(msg);
	close(recv_sock);
	return 0;
}