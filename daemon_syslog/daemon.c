#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include <syslog.h>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>

const char *IP_ADDR;
#define PORT 55555
#define TIME 600
#define LOG_FILE "/var/log/daemon_mysys.log"

int Daemon();
char *getTime();
int writeLog(char msg[256]);
void *send_log(void *arg);

int main(int argc, char *argv[]) {

	if( argc!=2 ){
		printf("Usage:  %s ip_address\n", argv[0]);
		exit(1);
	}

	IP_ADDR = argv[1];

 	writeLog("Daemon Start");

    pid_t parpid, sid;
    
    parpid = fork();
    if(parpid < 0) {
        exit(1);
    } else if(parpid != 0) {
        exit(0);
    } 
    umask(0);
    sid = setsid();
    if(sid < 0) {
        exit(1);
    }
    if((chdir("/")) < 0) {
        exit(1);
    }
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    
    return Daemon();	
}

int writeLog(char msg[3000]) { 
    FILE *pLog;
    pLog = fopen(LOG_FILE, "a");
    if(pLog == NULL) {
        return 1;
    }
    char str[3000];
    memset(str, 0, 3000);
    strcat(str, msg);
    strcat(str, "\n");
    fputs(str, pLog);
    fclose(pLog);
    return 0;
}

int Daemon(){
	FILE *info_file;
	char *buf_str;
	char *buf_time;
	int result;
	pthread_t thread;
	
	buf_time=calloc(300, sizeof(char));
	buf_str=calloc(3000, sizeof(char));

	result = pthread_create(&thread, NULL, send_log, NULL);
	if (result != 0) {
			writeLog("ERROR: no creating thread");
			
	}else {
			pthread_detach(thread);
	}

		while(1){

    	writeLog(getTime());
    	info_file = fopen("/proc/uptime", "r");	
		writeLog("\n+++++++++++++++++++++++ UPTIME +++++++++++++++++++++++\n");
		fread(buf_str, sizeof(char), 1500, info_file);		
		writeLog("| seconds of work up | (seconds of inaction)*(number of CPU) |\n");
		writeLog(buf_str);    	
    	fclose(info_file);

		memset(buf_str, 0, 3000);
		info_file = fopen("/proc/meminfo", "r");	
		writeLog("\n+++++++++++++++++++++++ MEMORY +++++++++++++++++++++++\n");
		fread(buf_str, sizeof(char), 1500, info_file);
    	writeLog(buf_str);    	
    	fclose(info_file);

    	memset(buf_str, 0, 3000);
    	info_file = fopen("/proc/loadavg", "r");	
		writeLog("\n+++++++++++++++++++++++ LOAD AVERAGE +++++++++++++++++++++++\n");
		writeLog("|load for 1min | load for 5min | load for 15min | active processes/number of processes | PID of last process|\n");
		fread(buf_str, sizeof(char), 1500, info_file);
    	writeLog(buf_str);    	
    	fclose(info_file);

		sleep(TIME);
	}
	free(buf_str);
	free(buf_time);
return 0;
}

char *getTime() { 
    time_t now;
    struct tm *ptr;
    static char tbuf[64];
    memset(tbuf, 0, 64);
    time(&now);
    ptr = localtime(&now);
    strftime(tbuf,64, "%Y-%m-%e %H:%M:%S", ptr);
    return tbuf;
}


void *send_log(void *arg){
	FILE *pLog;
	struct sockaddr_in ServAddr;
	struct sockaddr_in ClntAddr;
	char *Buffer; 
	int send_sock;
	int clnt_sock;
	int f_block_sz;
	unsigned int clntLen;
	Buffer=calloc(512, sizeof(char));
	
	
	if ((send_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		writeLog("ERROR: socket() failed");
	}

	memset(&ServAddr, 0, sizeof(ServAddr));
	ServAddr.sin_family = AF_INET;
	ServAddr.sin_port = htons(PORT);
	ServAddr.sin_addr.s_addr  = inet_addr(IP_ADDR);

	if (bind(send_sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0) {
		writeLog("ERROR: bind() failed");
	}
	
	
	listen(send_sock, 5);
	for(;;){
		clntLen = sizeof(ClntAddr);
		clnt_sock = accept(send_sock, (struct sockaddr *) &ClntAddr, &clntLen);
		f_block_sz=0;
		memset(Buffer, 0, (512*(sizeof(char))));
		pLog = fopen(LOG_FILE, "r");
		do{
			f_block_sz = fread(Buffer, sizeof(char), 512, pLog);
			
			
                send(clnt_sock, Buffer, f_block_sz, 0);
                memset(Buffer, 0, (512*(sizeof(char))));
        } while(f_block_sz  == 512);
		close(clnt_sock);
	}
	
	free(Buffer);
}