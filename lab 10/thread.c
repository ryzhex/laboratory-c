#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>
#include <errno.h>
#define Y 10
#define X 10
#define COUNT_TARGET 10

struct DATA {
	int **m;
};
typedef struct DATA Data;

int count_target_found;

int **map_filling();

void airport(int count_plane);

void *plane(void *arg);


int main(int argc, char const *argv[]) {

	if (argc < 2 || argc > 2) {
		printf("Usage: ./thread count_plane\n");
		exit(-1);
	}
	int count_plane = atoi(argv[1]);
	count_target_found = 0;
	airport(count_plane);

	return 0;
}


void airport(int count_plane) {
	Data data;
	int **map;
	pthread_t tid[count_plane];
	int result;
	printf("Карта: %dх%d \n", Y, X);
	map = map_filling();
	data.m = map;

	for (int i = 0; i < count_plane; ++i) {
		result = pthread_create( &tid[i], NULL, plane, &data);
		if (result != 0) {
			exit(-1);
		}
	}
	for (int i = 0; i < count_plane; ++i) {
		result = pthread_join(tid[i], NULL);
		if (result != 0) {
			exit(-1);
		}
	}
	printf("planes нашли целей %d\n",  count_target_found);



}

int **map_filling() {
	int **map;
	int *map_str;
	srand(getpid());

	map = calloc(Y, sizeof(map_str));
	for (int i = 0; i < Y; ++i)
	{
		map[i] = calloc(X, sizeof(int));
	}

	for (int i = 0; i < Y; ++i) {
		for (int j = 0; j < X; ++j)	{
			map[i][j] = 0;

		}
	}
	printf("Цели:  ");
	for (int i = 0; i < COUNT_TARGET; ++i)	{
		int y_target = rand() % Y;
		int x_target = rand() % X;
		map[y_target][x_target] = 1;
		printf("№%d [%d,%d] ", i, y_target, x_target);
	}
	printf("\n");

	return map;
}

void *plane(void *arg) {
	Data* data = (Data*)arg;
	int **map = data->m;
	int y_plane = rand() % Y;
	int x_plane = rand() % X;



	printf("координаты самолёта: %dx%d \n", y_plane, x_plane );
	int direction =(rand() % 4);

	printf("направление %d\n", direction );
	
	if (direction == 0) {
		for (int i = y_plane; i < Y; ++i) {
			if (map[i][x_plane] == 1) {
				printf("Цель найдена! координаты: [%d,%d] \n", i, x_plane );
				count_target_found = count_target_found + 1;
			}
		}
	} else if (direction == 1) {
		for (int i = x_plane; i < X; ++i) {
			if (map[y_plane][i] == 1) {
				printf("Цель найдена! координаты: [%d,%d] \n", y_plane, i );
				count_target_found = count_target_found + 1;
			}
		}
	} else if (direction == 2) {
		for (int i = y_plane; i >= Y; --i) {
			if (map[i][x_plane] == 1) {
				printf("Цель найдена! координаты: [%d,%d] \n", i, x_plane );
				count_target_found = count_target_found + 1;
			}
		}
	} else if (direction == 3) {
		for (int i = x_plane; i >= X; --i) {
			if (map[y_plane][i] == 1) {
				printf("Цель найдена! координаты: [%d,%d] \n", y_plane, i );
				count_target_found = count_target_found + 1;
			}
		}
	}

	pthread_exit(NULL);
}