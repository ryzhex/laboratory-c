#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h>


typedef struct item it;

struct item {
	int data;
	it *next;
	it *back;
};
it *addLast(it *head, int data) {


	while (head->next != NULL) {
		head = head->next;
	}

	it *last_item;
	last_item = malloc(sizeof(it));

	last_item->next = NULL;

	last_item->back = head;
	head->next = last_item;
	last_item->data = data;

	return last_item;
}

int delete(it *head, int data) {

	it *pre_item = NULL;
	it *ove_item = NULL;

	while (head->data != data) {
		if (head->next == NULL) {
			printf("error\n");
			return -1;
		}
		pre_item = head;
		head = head->next;

	}
	if (head->back == NULL) {
		// printf("1\n");
		ove_item = head->next;
		head->data = ove_item->data;
		head->next = ove_item->next;
		head->back = NULL;

		free(ove_item);

	} else if (head->next == NULL) {
		// printf("2\n");
		pre_item = head->back;
		pre_item->next = NULL;
		free(head);
	} else {
		// printf("3\n");
		pre_item->next = head->next;
		ove_item = head->next;
		ove_item->back = pre_item;
		free(head);

	}

	printf("delete success\n");
	return 0;
}

void showlist(it *cur) {
	int i = 0;
	while (cur->next != NULL) {

		printf("Struct №%d: data=%d\n", i, cur->data);
		cur = cur->next;
		i++;

	}
	printf("Struct №%d: data=%d\n", i, cur->data);

}

it *addFirst(it *head, int data) {

	it *buf_item;
	buf_item = malloc(sizeof(it));
	buf_item->data = head->data;
	buf_item->next = head->next;
	buf_item->back = head;

	head->next = buf_item;
	head->data = data;

	return buf_item;
}

it *insAfterItem(it *head, int data, int nom) {
	if (nom == 0) {
		return addFirst(head, data);
	}

	it *buf_item;

	int i = 0;

	while (i != nom) {
		head = head->next;
		i++;
		if (head->next == NULL)	{
			if (i != nom) {
				printf("Не правильное указание точки создания\n");
				return NULL;
			}
			return addLast(head, data);
		}

	}


	it *ins_item;
	ins_item = malloc(sizeof(it));

	buf_item = head->next;

	buf_item->back = ins_item;
	head->next = ins_item;

	ins_item->back = head;
	ins_item->next = buf_item;
	ins_item->data = data;
	return ins_item;
}

void free_memory(it *head) {
	while (head->next != NULL) {
		head = head->next;
		free(head->back);
	}

}

int main(int argc, char const *argv[])
{
	it *item_head;
	item_head = malloc(sizeof(it));
	item_head->data = 1312;
	item_head->next = NULL;
	item_head->back = NULL;
	mtrace();
	for (int i = 0; i < 6; ++i) {
		addLast(item_head, i + 1313);
	}
	showlist(item_head);
	// printf("back%d next%d\n",item_head.back,item_head.next);
	delete(item_head, 1312);
	showlist(item_head);
	delete(item_head, 1316);
	showlist(item_head);
	delete(item_head, 1318);
	showlist(item_head);
	printf("wddwd\n");
	insAfterItem(item_head, 131111, 4);
	showlist(item_head);
	free_memory(item_head);

	return 0;
}