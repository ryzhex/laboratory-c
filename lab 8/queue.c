#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>
#include <signal.h>
#define MAX_GOLD 1500
#define MSGPERM 0600
#define RATE 25



struct question {
	long mtype;
	int num_unit;
	int time_m;
};

void miners(int msgqid, int i);
void mine(int count_unit);



int main(int argc, char *argv[]) {

	if (argc < 1) {
		printf("Usage: ./queue count_unit\n");
		exit(-1);
	}

	int count_unit = atoi(argv[1]);

	mine(count_unit);

	return 0;
}



void miners(int msgqid, int i) {
	unsigned int time_mining = 0;
	struct question buf_out;
	srand(getpid());
	int rc;


	while (1) {
		time_mining = (rand() % 10);
		usleep(time_mining);

		buf_out.mtype = 1;
		buf_out.num_unit = i;
		buf_out.time_m = time_mining;

		rc = msgsnd(msgqid, &buf_out, sizeof(buf_out) - sizeof(long), 0);
		if (rc < 0) {
			perror( strerror(errno) );
			printf("msgsnd failed, i= %d,rc_out = %d\n", i, rc);
			exit(1);
		}

	}
}



void mine(int count_unit) {
	int msgqid, rc;
	int count_gold = MAX_GOLD;
	int stat;
	pid_t pid[count_unit];
	key_t MSGKEY = ftok("/etc/drirc", 0);
	struct question buf_in;

	
	msgqid = msgget(MSGKEY, MSGPERM | IPC_CREAT | IPC_EXCL);
	if (msgqid < 0) {
		perror(strerror(errno));
		printf("failed to create message queue with msgqid_que = %d\n", msgqid);
		exit(1);
	}

	for (int i = 0; i < count_unit; i++) {
		pid[i] = fork();
		if (-1 == pid[i]) {
			perror("fork");
			exit(1);
		} else if (0 == pid[i]) {
			miners(msgqid, i);
		}
	}

	while (1) {
		rc = msgrcv(msgqid, &buf_in, sizeof(buf_in) - sizeof(long), 1, 0);
		if (rc > 0) {
			count_gold = count_gold - RATE;

			printf("Otchet:\n");
			printf("unit №=%d time sleep=%d count gold balance=%d \n", buf_in.num_unit, buf_in.time_m, count_gold);

			if (count_gold <= 0) {
				for (int i = 0; i < count_unit; ++i) {
					kill(pid[i], SIGTERM);
				}


				break;
			}
		}
	}

	for (int i = 0; i < count_unit; ++i) {
		waitpid(pid[i], &stat, 0);
	}
	rc = msgctl(msgqid, IPC_RMID, NULL);
}

