#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>

void read_write_file(FILE* flinp, char simbol) {
    FILE* flout;
    char ch;
    while ((ch = fgetc(flinp)) != EOF) {
        if (ch != simbol) {
            if ((flout = fopen("file.txt.out", "a")) == NULL) {
                printf("Не удается открыть файл.\n");
                exit(1);
            }

            fputc(ch, flout);

            if (fclose(flout)) {
                printf("Ошибка при закрытии файла.\n");
                exit(1);
            }
        }

    }
};





int main(int argc, char **argv) {
    if (argc != 5) {
        printf("Кол-во параметров должно быть два\n");
        exit(1);
    }
    int opt = 0;
    printf("%s ", argv[0]);
    char* f;
    FILE* fp;
    f = malloc(sizeof(char) * 100);
    char ch = NULL;

    while ( (opt = getopt(argc, argv, "c:f:")) != -1) {
        switch (opt) {

        case 'f':
            strcpy(f, optarg);
            break;

        case 'c':
            ch = optarg[0];
            break;

        default:
            fprintf(stderr, "Использование : %s [-f имя входнлго файла] [-l длина строки]\n", argv[0]);
            exit(EXIT_FAILURE);


        }
        printf("-%c %s ", opt, optarg);
    };


    if ((fp = fopen(f, "r")) == NULL) {
        printf("Не удается открыть файл.\n");
        exit(1);
    }


    read_write_file(fp, ch);

    if (fclose(fp)) {
        printf("Ошибка при закрытии файла.\n");
        exit(1);
    }
    free(f);
    return 0;
}