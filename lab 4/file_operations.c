#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#define len 100
void read_write_file(FILE* flinp, int l,  char* address) {
    FILE* flout;
    char out_str[len];
    while (fgets(out_str, sizeof(out_str), flinp)) {
        printf("\n ? %s", out_str);
        if (out_str[l - 1] == '\n') {
            if ((flout = fopen("file.txt.out", "a")) == NULL) {
                printf("Не удается открыть файл.\n");
                exit(1);
            }

            printf("dwdwdwwdd\n");w
            fwrite(out_str, sizeof(char), strlen(out_str), flout);

            if (fclose(flout)) {
                printf("Ошибка при закрытии файла.\n");
                exit(1);
            }
        }

    }
};





int main(int argc, char **argv) {
    if (argc != 5) {
        printf("Кол-во параметров должно быть два\n");
        exit(1);
    }
    int opt = 0;
    printf("%s ", argv[0]);
    char* f;
    int l = 0;
    FILE* fp;
    f = malloc(sizeof(char) * 100);


    while ( (opt = getopt(argc, argv, "l:f:")) != -1) {
        switch (opt) {

        case 'f':
            strcpy(f, optarg);
            break;

        case 'l':
            l = atoi(optarg);
            break;

        default: /* '?' */
            fprintf(stderr, "Использование : %s [-f имя входнлго файла] [-l длина строки]\n", argv[0]);
            exit(EXIT_FAILURE);


        }
        printf("-%c %s ", opt, optarg);
    };


    if ((fp = fopen(f, "r")) == NULL) {
        printf("Не удается открыть файл.\n");
        exit(1);
    }


    read_write_file(fp, l, f);

    if (fclose(fp)) {
        printf("Ошибка при закрытии файла.\n");
        exit(1);
    }
    free(f);
    return 0;
}