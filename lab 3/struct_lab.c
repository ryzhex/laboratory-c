#include <stdio.h>
#include <stdlib.h>

struct Person {

    char FName[50];
    int burn_year;
    int salary;

};

typedef struct Person persons;

void inp_struct(persons* per) {
    printf("Введите имя:\n");
    scanf("%s", per->FName);
    printf("Введите год рождения:\n");
    scanf("%d", &per->burn_year);
    printf("Введите оклад:\n");
    scanf("%d", &per->salary);
}

void outp_struct(int num, persons* out_pers) {
    printf("Ввывод структур:\n");
    printf("Ввывод структурa номер %d: \n", num);
    printf("Фамилия: %s \n", out_pers->FName);
    printf("Год рождения: % d \n", out_pers->burn_year);
    printf("Оклад: % d \n", out_pers->salary);
}
int compare(const void* p1, const void* p2) {
    persons* st1 = *(persons**)p1;
    persons* st2 = *(persons**)p2;
    return st1->burn_year - st2->burn_year;
}

int main(int argc, char **argv) {
    int count = 0;
    printf("Введите количество структур: \n");
    scanf("%d", &count);

    persons** pers = malloc(sizeof(persons**) * count);

    for (int i = 0; i < count; ++i)
    {
        pers[i] = malloc(sizeof(persons));
        inp_struct(pers[i]);
    }

    printf("До сортировки: \n \n");

    for (int i = 0; i < count; ++i)
    {
        outp_struct(i + 1, pers[i]);
    }

    qsort(pers, count, sizeof(persons**), compare);

    printf("После сортировки: \n \n");

    for (int i = 0; i < count; ++i)
    {
        outp_struct(i + 1, pers[i]);
    }

    for (int i = 0; i < count; ++i) {
        free(pers[i]);
    }

    free(pers);
    return 0;
}


