#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

void vvod_m_v(int str, int clm, int* vector, int** matrix);

void schot(int m_str, int m_clm, int* vector, int** matrix);



int main(int argc, char *argv[]) {


	if (argc < 2) {
		printf("Usage: ./pipes count_matrix_string count_matrix_column\n");
		exit(-1);
	}

	int m_clm = atoi(argv[2]);
	int m_str = atoi(argv[1]);
	int** matrix;
	int* str_matrix;
	int* vector;

	vector = calloc(m_clm, sizeof(int));
	matrix = calloc(m_str, sizeof(str_matrix));
	for (int i = 0; i < m_str; ++i)
	{
		matrix[i] = calloc(m_clm, sizeof(int));
	}




	vvod_m_v(m_str, m_clm, vector, matrix);

	schot(m_str, m_clm, vector, matrix);


	// printf("m_str=%dxx\n", m_str );
	// printf("m_clm=%dxx\n", m_clm );



	return 0;
}

void vvod_m_v(int str, int clm, int* vector, int** matrix) {

	srand(getpid());
	printf("Matrix:\n");
	for (int i = 0; i < str ; i++)
	{
		for (int j = 0; j < clm; j++)
		{
			matrix[i][j] = rand() % 10;
			printf("%d   ", matrix[i][j]);
			vector[j] = rand() % 5;
		}
		printf("\n");


	}
	printf("Vector:\n");
	for (int i = 0; i < clm; i++)
	{
		printf("%d\n", vector[i]);
	}
}

void schot(int m_str, int m_clm, int* vector, int** matrix) {
	int status, stat;
	pid_t pid[m_str];
	int fd[m_str][2];
	int otvet_out = 0;
	for (int i = 0; i < m_str; i++) {
		pipe(fd[i]);
		pid[i] = fork();
		if (-1 == pid[i]) {
			perror("fork");
			exit(1);
		} else if (0 == pid[i]) {
			close(fd[i][0]);

			for (int k = 0; k < m_clm; ++k)
			{
				otvet_out += matrix[i][k] * vector[k];
			}

			write(fd[i][1], &otvet_out, sizeof(int));
			exit(0);
		}
	}
	printf("Otvet:\n");
	for (int i = 0; i < m_str; i++) {
		status = waitpid(pid[i], &stat, 0);
		if (pid[i] == status) {
			close(fd[i][1]);
			int otvet = 0;
			read(fd[i][0], &otvet, sizeof(int));
			printf("%d\n", otvet );
		}
	}
}