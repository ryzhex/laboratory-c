#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h>

#define MSGPERM 0600
#define START_PORT 12345
#define MAXPENDING 10
#define COUNT_PLAYER 3
char *BROAD;


static pthread_barrier_t barrier;

int flag_cancel;

struct Adress {
	unsigned short int port;
	struct in_addr ip_client;
};

struct msg_Q {
	long mtype;
	struct Adress kl_ad;
	struct Adress slf_ad;
};

struct ThreadArgs {
	struct Adress *client_addr;
	int clntSock;
	struct Adress self_addr;
	int msgqid;

};

void DieWithError(char *errorMessage) {
	fprintf (stderr, "errno = %d \n", errno);
	perror(errorMessage);
	exit(1);
};

void Udp_kill_win(struct Adress kill_addr, int target) {
	unsigned int StringLen;
	int sock;
	char *String;
	if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		DieWithError("socket() failed");
	}

	if (target == 0) {
		String = "Die";
	} else if (target == 1) {
		String = "Win";
	}

	struct sockaddr_in SockAddr;
	memset(&SockAddr, 0, sizeof(SockAddr));
	SockAddr.sin_family = AF_INET;
	SockAddr.sin_port = htons(kill_addr.port);
	SockAddr.sin_addr = kill_addr.ip_client;
	StringLen = strlen(String);
	if (sendto(sock, String, strlen(String), 0, (struct sockaddr *) &SockAddr, sizeof(SockAddr)) != StringLen) {
		DieWithError("send() failed\n");
	}
	printf("%s %s:%d\n", String, inet_ntoa(kill_addr.ip_client), kill_addr.port);
	close(sock);
}

void *brd_port(void *arg) {
	int i = *((int*)arg);
	int port = START_PORT + i;
	char *String = "Yes";
	unsigned int StringLen;
	int sock;

	if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		DieWithError("socket() failed");
	}
	printf("%d\n", port );
	int status = 1;
	setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &status, sizeof(status));
	struct sockaddr_in SockAddr;
	memset(&SockAddr, 0, sizeof(SockAddr));
	SockAddr.sin_family = AF_INET;
	SockAddr.sin_port = htons(port);
	SockAddr.sin_addr.s_addr = inet_addr(BROAD);
	StringLen = strlen(String);
	for (;;) {

		if (sendto(sock, String, strlen(String), 0, (struct sockaddr *) &SockAddr, sizeof(SockAddr)) != StringLen) {
			DieWithError("send() failed\n");
		}
		if (flag_cancel == 1) {
			close(sock);
			pthread_exit(NULL);
		}
		sleep(1);
	}

}

void *tcp_client(void *arg) {
	int count_enemy = COUNT_PLAYER - 1;
	int client_Sock = ((struct ThreadArgs *) arg) -> clntSock;
	int msgqid = ((struct ThreadArgs *) arg) -> msgqid;
	struct Adress *players_addrs = ((struct ThreadArgs *) arg) -> client_addr;
	struct Adress self_addr = ((struct ThreadArgs *) arg) -> self_addr;
	struct Adress *enemy;
	struct msg_Q buf_out;
	if ((enemy = (struct Adress *)calloc(count_enemy , sizeof(struct Adress))) == NULL) {
		DieWithError("calloc() failed");
	}

	int status = pthread_barrier_wait(&barrier);
	if (status == PTHREAD_BARRIER_SERIAL_THREAD) {
		pthread_barrier_destroy(&barrier);
	} else if (status != 0) {
		printf("error wait barrier in main thread with status = %d\n", status);
		exit(-1);
	}
	if (send(client_Sock, &self_addr , sizeof(struct Adress), 0) != sizeof(struct Adress) ) {
		DieWithError("send() failed");
	}

	for (;;) {
		int count_enemy = 0;
		for (int i = 0; i < COUNT_PLAYER; i++) {
			// printf("srav %d ip1 %s ip2 %s\n", strcmp(inet_ntoa(players_addrs[i].ip_client),inet_ntoa(self_addr.ip_client)),inet_ntoa(players_addrs[i].ip_client), inet_ntoa(self_addr.ip_client));

			if ((players_addrs[i].port != self_addr.port) && (players_addrs[i].port != 0) && (!(strcmp(inet_ntoa(players_addrs[i].ip_client), inet_ntoa(self_addr.ip_client))))) {
				enemy[count_enemy].port = players_addrs[i].port;
				enemy[count_enemy].ip_client = players_addrs[i].ip_client;
				count_enemy++;
				printf("dwdwd %d\n", count_enemy);

			}
		}
		if (count_enemy == 0) {
			close(client_Sock);
			pthread_exit(NULL);
		}

		if (send(client_Sock, &count_enemy , sizeof(count_enemy), 0) != sizeof(count_enemy) ) {
			DieWithError("send() failed");
		}
		for (int i = 0; i < count_enemy; ++i)
		{

			printf("otpravka vragov %d %s\n", enemy[i].port, inet_ntoa(enemy[i].ip_client) );
		}
		if (send(client_Sock, enemy, (count_enemy * sizeof(struct Adress)), 0) != (count_enemy * sizeof(struct Adress))) {
			DieWithError("send() failed");
		}
		if ((recv(client_Sock, &buf_out.kl_ad, sizeof(struct Adress), 0)) < 0) {
			DieWithError("recv() failed");
		}
		// printf("kill ip: %ld port: %d\n", buf_out.kl_ad.ip_client, buf_out.kl_ad.port);

		buf_out.mtype = 1;
		buf_out.slf_ad = self_addr;
		int rc = msgsnd(msgqid, &buf_out, sizeof(buf_out) - sizeof(long), 0);
		if (rc < 0) {
			perror( strerror(errno) );
			printf("msgsnd failed, rc_out = %d\n", rc);
			exit(1);
		}
	}

}

void Game(int count_client, pthread_t threads[]) {

	int msgqid;
	int servSock;                    /* Socket descriptor for server */
	int clntSock;                    /* Socket descriptor for client */
	struct sockaddr_in echoServAddr; /* Local address */
	struct sockaddr_in echoClntAddr; /* Client address */
	unsigned int clntLen;            /* Length of client address data structure */
	struct ThreadArgs *threadArgs;
	struct Adress *adresses;
	key_t MSGKEY = ftok("/etc/drirc", 0);
	int result;
	pthread_t thread;

	msgqid = msgget(MSGKEY, MSGPERM | IPC_CREAT | IPC_EXCL);
	if (msgqid < 0) {
		perror(strerror(errno));
		printf("failed to create message queue with msgqid_que = %d\n", msgqid);
		exit(1);
	}
	flag_cancel = 0;
	int status = pthread_barrier_init(&barrier, NULL, COUNT_PLAYER + 1);
	if (status != 0) {
		printf("main error: can't init barrier, status = %d\n", status);
		exit(-1);
	}

	if ((adresses = (struct Adress *)calloc(COUNT_PLAYER, sizeof(struct Adress))) == NULL) {
		DieWithError("calloc() failed");
	}

	if ((servSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		DieWithError("socket() failed");
	}

	memset(&echoServAddr, 0, sizeof(echoServAddr));
	echoServAddr.sin_family = AF_INET;
	echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	echoServAddr.sin_port = htons(START_PORT);

	if (bind(servSock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0) {
		DieWithError("bind() failed");
	}

	if (listen(servSock, MAXPENDING) < 0) {
		DieWithError("listen() failed");
	}

	for (int i = 0; i < COUNT_PLAYER; i++) {

		clntLen = sizeof(echoClntAddr);

		fflush(stdout);
		if ((clntSock = accept(servSock, (struct sockaddr *) &echoClntAddr, &clntLen)) < 0)	{
			DieWithError("accept() failed");
		}

		printf("Handling client %s\n", inet_ntoa(echoClntAddr.sin_addr));
		printf("port client %d\n", echoClntAddr.sin_port);
		adresses[i].port = echoClntAddr.sin_port;
		adresses[i].ip_client = echoClntAddr.sin_addr;

		if ((threadArgs = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs))) == NULL) {
			DieWithError("malloc() failed");
		}

		threadArgs -> clntSock = clntSock;
		threadArgs -> client_addr = adresses;
		threadArgs ->self_addr.port = echoClntAddr.sin_port;
		threadArgs ->self_addr.ip_client = echoClntAddr.sin_addr;
		threadArgs ->msgqid = msgqid;

		result = pthread_create(&thread, NULL, tcp_client, threadArgs);
		if (result != 0) {
			perror("Creating the first thread");
			exit(-1);
		}
	}

	status = pthread_barrier_wait(&barrier);
	if (status == PTHREAD_BARRIER_SERIAL_THREAD) {
		pthread_barrier_destroy(&barrier);
	} else if (status != 0) {
		printf("error wait barrier in main thread with status = %d\n", status);
		exit(-1);
	}
	flag_cancel = 1;

	int rc;
	struct msg_Q buf_in;
	int allive_players = COUNT_PLAYER;

	for (;;) {

		rc = msgrcv(msgqid, &buf_in, sizeof(buf_in) - sizeof(long), 0, 0);
		if (rc > 0) {
			for (int i = 0; i < COUNT_PLAYER; ++i) {
				if ((adresses[i].port == buf_in.slf_ad.port) /*&& (!(adresses[i].ip_client == buf_in.slf_ad.ip_client))*/) {

					for (int k = 0; k < COUNT_PLAYER; ++k) {
						if ((adresses[k].port == buf_in.kl_ad.port)/* && (!(adresses[k].ip_client == buf_in.kl_ad.ip_client))*/) {

							adresses[k].port = 0;
							adresses[k].ip_client.s_addr = 0;
							allive_players = allive_players - 1;
							Udp_kill_win(buf_in.kl_ad, 0);
							if (allive_players == 1) {
								Udp_kill_win(buf_in.slf_ad, 1);
								printf("END\n");
								rc = msgctl(msgqid, IPC_RMID, NULL);
								exit(0);
							}
						}
					}
				}
			}
		}

	}

}



void invitation(int count_client) {
	int t[count_client];
	int result;
	pthread_t threads[count_client];

	for (int i = 0; i < count_client; i++) {
		t[i] = i;
		result = pthread_create(&threads[i], NULL, brd_port, &t[i]);
		if (result != 0) {
			perror("Creating the first thread");
			exit(-1);
		} else {
			pthread_detach(threads[i]);
		}
	}

	Game(count_client, threads);
};


int main(int argc, char const *argv[]) {
	if (argc < 3) {
		fprintf(stderr, "Usage:  %s count_client ip_broadcast\n", argv[0]);
		exit(1);
	}


	int count_client = atoi(argv[1]);
	BROAD = argv[2];
	invitation(count_client);


	return 0;
}
