#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <errno.h>
#include <pthread.h>

#define ADDR_BUF 10
#define END_PORT 12355
#define START_PORT 12345
#define BUF_MAX 255

int the_end;

struct Adress {
	unsigned short int port;
	struct in_addr ip_client;
};

void DieWithError(char *errorMessage) {
	perror(errorMessage);
	exit(1);
};

void *udp_live(void *arg) {
	struct Adress *serv_addr = ((struct Adress *) arg);
	int sock_end;
	int respStringLen;
	char Buffer[3] = "";
	struct sockaddr_in fromAddr;
	unsigned int fromSize;
	int port = serv_addr->port + 1;

	if ((sock_end = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		DieWithError("socket() failed");
	}
	printf("%d\n", serv_addr->port);
	struct sockaddr_in SockAddr;
	memset(&SockAddr, 0, sizeof(SockAddr));
	SockAddr.sin_family = AF_INET;
	SockAddr.sin_port = port;
	SockAddr.sin_addr = serv_addr->ip_client;

	while (bind(sock_end, (struct sockaddr *) &SockAddr, sizeof(SockAddr)) < 0) {
		printf("smena porta\n");
		port = port + 1;
		SockAddr.sin_port = htons(port);
		if (port == END_PORT) {
			DieWithError("bind() failed");
		}

	}

	fromSize = sizeof(fromAddr);
	printf("dwdwdwdwd\n");
	if ((respStringLen = recvfrom(sock_end, Buffer, sizeof(Buffer), 0, (struct sockaddr *) &fromAddr, &fromSize)) < 0) {
		DieWithError("recvfrom() failed");
	}
	printf("cxcxcxcxcx\n");
	if (sendto(sock_end, Buffer, respStringLen, 0, (struct sockaddr *) &fromAddr, sizeof(fromAddr)) != respStringLen) {
		DieWithError("sendto() sent a different number of bytes than expected");
	}
	printf("kmkmmkmkmkm\n");
	printf("end %s\n", Buffer );
	close(sock_end);
	the_end = 1;
	return NULL;

}

struct Adress socket_listen() {
	struct Adress addr_serv_tcp;
	int sock;
	char Buffer[BUF_MAX + 1] = "";
	int port = START_PORT;
	struct sockaddr_in fromAddr;
	unsigned int fromSize;
	int respStringLen;


	if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		DieWithError("socket() failed");
	}

	struct sockaddr_in SockAddr;
	memset(&SockAddr, 0, sizeof(SockAddr));
	SockAddr.sin_family = AF_INET;
	SockAddr.sin_port = htons(port);
	SockAddr.sin_addr.s_addr = htonl(INADDR_ANY);

	while (bind(sock, (struct sockaddr *) &SockAddr, sizeof(SockAddr)) < 0) {
		printf("smena porta\n");
		port = port + 1;
		SockAddr.sin_port = htons(port);
		if (port == END_PORT) {
			close(sock);
			DieWithError("bind() failed");
		}

	}


	fromSize = sizeof(fromAddr);
	if ((respStringLen = recvfrom(sock, Buffer, BUF_MAX, 0, (struct sockaddr *) &fromAddr, &fromSize)) < 0) {
		DieWithError("recvfrom() failed");
	}

	printf("connect: %s\n", Buffer);
	printf("%s\n", inet_ntoa(fromAddr.sin_addr));
	printf("%d\n", fromAddr.sin_port);


	addr_serv_tcp.port = fromAddr.sin_port;
	addr_serv_tcp.ip_client = fromAddr.sin_addr;
	close(sock);
	return addr_serv_tcp;
}

void socket_send(struct Adress addr_serv_tcp) {

	int send_sock;
	int count_enemy;
	srand(getpid());
	struct Adress *adresses;
	pthread_t thread;
	int result;
	struct Adress udp_life_addr;


	if ((send_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		DieWithError("socket() failed");
	}

	struct sockaddr_in ServAddr;
	memset(&ServAddr, 0, sizeof(ServAddr));
	ServAddr.sin_family = AF_INET;
	ServAddr.sin_port = htons(START_PORT);
	ServAddr.sin_addr = addr_serv_tcp.ip_client;
	printf("%s\n", inet_ntoa(addr_serv_tcp.ip_client));
	if (connect(send_sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0) {
		DieWithError("connect() failed");
	}
	if ((recv(send_sock, &udp_life_addr, sizeof(struct Adress), 0)) < 0) {
		DieWithError("recv() failed");
	}
	udp_life_addr.ip_client = addr_serv_tcp.ip_client;

	result = pthread_create(&thread, NULL, udp_live, &udp_life_addr);
	if (result != 0) {
		perror("Creating the first thread");
		exit(-1);
	} else {
		pthread_detach(thread);
	}
	if ((adresses = (struct Adress *)calloc(count_enemy, sizeof(struct Adress))) == NULL) {
		DieWithError("calloc() failed");
	}
	for (;;) {
		if (the_end == 1) {
			printf("END\n");
			close(send_sock);
			exit(0);
		}
		if ((recv(send_sock, &count_enemy, sizeof(count_enemy), 0)) < 0) {
			DieWithError("recv() failed");
		}



		if ((recv(send_sock, adresses, (count_enemy * sizeof(struct Adress)), 0)) < 0) {
			DieWithError("recv() failed");
		}
		int id_kill = rand() % count_enemy;
		// printf("id_kill %d\n", id_kill);
		if (send(send_sock, &adresses[id_kill], sizeof(struct Adress), 0) != sizeof(struct Adress)) {
			DieWithError("send() failed");
		}
		printf("1\n");
	}

}


int main(int argc, char const *argv[]) {
	the_end = 0;
	struct Adress addr_serv_tcp;
	addr_serv_tcp = socket_listen();
	socket_send(addr_serv_tcp);
	printf("END2\n");
	return 0;
}
