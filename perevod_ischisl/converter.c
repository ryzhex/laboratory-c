#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#define MAX_LEN 30
void hendler(int chislo, int system);

int main(int argc, char const *argv[]) {
	if (argc < 3) {
		fprintf(stderr, "Usage:  %s chislo system(2,8,16)\n", argv[0]);
		exit(1);
	}
	int ch = atoi(argv[1]);
	int sys = atoi(argv[2]);
	hendler(ch, sys);
	switch (sys) {
	case 8:
		printf("\nПРОВЕРКА\n");
		printf("%o\n", ch);
		break;
	case 16:
		printf("\nПРОВЕРКА\n");
		printf("%X\n", ch);
		break;
	}
	return 0;
}

void hendler(int chislo, int system) {
	int *buffer;
	int ostatok;
	int count = 0;

	buffer = calloc(MAX_LEN, sizeof(int));

	do {
		ostatok = chislo % system;
		chislo = (chislo - ostatok) / system;
		buffer[count] = ostatok;
		count++;

	} while (chislo != 0);
	for (int i = count - 1; i >= 0; i-- )
	{
		if (system == 16) {
			if (buffer[i] > 9) {
				switch (buffer[i]) {
				case 10:
					printf("A");
					break;
				case 11:
					printf("B");
					break;
				case 12:
					printf("C");
					break;
				case 13:
					printf("D");
					break;
				case 14:
					printf("E");
					break;
				case 15:
					printf("F");
					break;
				}
			} else {
				printf("%d", buffer[i]);
			}

		} else {
			printf("%d", buffer[i]);
		}
	}
}

