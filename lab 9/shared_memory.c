#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/errno.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <signal.h>

#define EAT_VINNY 50
#define MINE_BEE 15

union semun {
	int val;                  /* значение для SETVAL */
	struct semid_ds *buf;     /* буферы для  IPC_STAT, IPC_SET */
	unsigned short *array;    /* массивы для GETALL, SETALL */
	/* часть, особенная для Linux: */
	struct seminfo *__buf;    /* буфер для IPC_INFO */
};


void bee(int count_bee, int shmid, int semid);
void vinny(int shmid, int semid);
void beehive(int count_bee);



int main(int argc, char *argv[]) {


	if (argc < 2 || argc > 2) {
		printf("Usage: ./shared_memory count_bee\n");
		exit(-1);
	}
	int count_bee = atoi(argv[1]);

	beehive(count_bee);

	return 0;
}



void bee(int count_bee, int shmid, int semid) {
	unsigned int time_mining = 0;
	srand(getpid());
	int* honey;

	struct sembuf lock_res = {0, -1, 0};	//блокировка ресурса
	struct sembuf rel_res = {0, 1, 0};	//освобождение ресурса

	while (1) {
		time_mining = (rand() % 10);
		usleep(time_mining);

		if ((honey = (int*)shmat(shmid, NULL, 0)) == (int *) - 1) {
			perror("shmat");
			exit(1);
		}

		if ((semop(semid, &lock_res, 1)) == -1) {
			exit(1);
		}

		*(honey) = *(honey) + MINE_BEE;
		printf("After bee count honey=%d\n", *(honey) );

		if ((semop(semid, &rel_res, 1)) == -1) {
			exit(1);
		}

	}
}



void vinny(int shmid, int semid) {
	int* honey;
	unsigned int time_mining = 0;
	srand(getpid());

	struct sembuf lock_res = {0, -1, 0};	//блокировка ресурса
	struct sembuf rel_res = {0, 1, 0};	//освобождение ресурса


	while (1) {
		time_mining = 10 + (rand() % 15);
		usleep(time_mining);

		if ((honey = (int*)shmat(shmid, NULL, 0)) == (int *) - 1) {
			perror("shmat");
			exit(1);
		}

		if ((semop(semid, &lock_res, 1)) == -1) {
			exit(1);
		}



		if (*(honey) < 50) {
			printf("Vinny R.I.P.\n");
			exit(0);

		} else {

			if (*(honey) > 1000)
			{
				printf("Vinny will never die\n");
				exit(0);
			}
			*(honey) = *(honey) - EAT_VINNY;
			printf("After vinny count honey=%d\n", *(honey) );
			if ((semop(semid, &rel_res, 1)) == -1) {
				exit(1);
			}

		}




	}
}



void beehive(int count_bee) {
	union semun arg;
	int stat, shmid, semid;
	int *mem;
	int honey = 75;
	pid_t pid[count_bee];
	pid_t vinny_pid;
	key_t MSGKEY = ftok("/etc/drirc", 0);

	struct sembuf lock_res = {0, -1, 0};	//блокировка ресурса
	struct sembuf rel_res = {0, 1, 0};	//освобождение ресурса

	if ((shmid = shmget(MSGKEY, sizeof(int), 0666 | IPC_CREAT )) < 0) {
		perror("shmget");
		exit(1);
	}

	if ((semid = semget(MSGKEY, 1, 0666 | IPC_CREAT )) < 0) {
		perror("semget");
		exit(1);
	}

	arg.val = 1;
	semctl(semid, 0, SETVAL, arg);

	if ((mem = (int*)shmat(shmid, NULL, 0)) == (int *) - 1) {
		perror("shmat");
		exit(1);
	}

	if ((semop(semid, &lock_res, 1)) == -1) {
		exit(1);
	}


	*(mem) = honey;

	if ((semop(semid, &rel_res, 1)) == -1) {
		exit(1);
	}


	vinny_pid = fork();
	if (-1 == vinny_pid) {
		perror("vinny fork");
		exit(1);
	} else if (0 == vinny_pid) {
		vinny(shmid, semid);
	}

	for (int i = 0; i < count_bee; i++) {
		pid[i] = fork();
		if (-1 == pid[i]) {
			perror("bee fork");
			exit(1);
		} else if (0 == pid[i]) {
			bee(count_bee, shmid, semid);
		}
	}




	waitpid(vinny_pid, &stat, 0);

	for (int i = 0; i < count_bee; ++i)
	{
		kill(pid[i], SIGKILL);
		printf("kill bee №%d\n", i);
	}

	if (shmctl(shmid, IPC_RMID, 0) < 0) {
		printf("Невозможно удалить область\n");
		exit(1);
	} else
		printf("Сегмент памяти помечен для удаления\n");

	if (semctl(semid, 0, IPC_RMID) < 0) {
		printf("Невозможно удалить семафор\n");
		exit(1);
	}
}

