#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <stdlib.h>


int maxlen = 0;
int countstr = 0;

int compare(const void * x1, const void * x2) {
	int len1 = strlen(*(char**)x1);
	int len2 = strlen(*(char**)x2);

	return (len1 - len2);
}

char *inp_str(int index, int maxlen) {
	char *q_one;
	char format[80];

	q_one = (char*)malloc(maxlen * sizeof(char));
	printf("введите %d строку \n", index);
	sprintf(format, "%%%ds", maxlen);
	scanf(format, q_one);

	return q_one;
}

int main(int argc, char *argv[]) {
	printf("введите максимальную длину строк \n");
	scanf("%d", &maxlen);

	printf("введите количество строк \n");
	scanf("%d", &countstr);

	char **q = malloc(countstr * sizeof(char*));
	for (int i = 0; i < countstr; i++) {
		q[i] = inp_str(i, maxlen);
	}

	printf("Массив до сортировки: \n");
	for (int i = 0; i < countstr; i++) {
		printf("%s \n", q[i]);
	}

	qsort(q, countstr, sizeof(char**), compare);

	printf("Массив после сортировки: \n");
	for (int i = 0; i < countstr; i++) {
		printf("%s \n", q[i]);
	}




	for (int j = 0; j < maxlen; j++) {
		free(q[j]);
	}

	free(q);

	return 0;
}
