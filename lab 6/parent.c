#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define buf 100

void read_file(char* str_poisk, char string_in[]) {
	int status;
	pid_t pid = fork();
	pid_t w;
	if (-1 == pid ) {
		printf("Ошибка при создании потомка\n");
		exit(1);
	}
	if (0 == pid) {


		if (string_in != NULL) {
			printf(" CHILD: Это процесс-потомок!\n");
			printf(" CHILD: Мой PID -- %d\n", getpid());
			printf(" CHILD: PID моего родителя -- %d\n", getppid());
			printf("vhod: %s  poisk: %s \n", string_in, str_poisk);
			if (strcmp(string_in, str_poisk) == 0) {
				printf("строка найдена \n \n");
				_exit(EXIT_SUCCESS);
			} else {
				printf("строка не найдена \n \n");
				_exit(EXIT_FAILURE);
			}
		}
	} else {
		do {
			w = waitpid(pid, &status, WUNTRACED | WCONTINUED);
			if (-1 == w) {
				perror("waitpid");
				exit(EXIT_FAILURE);
			}
		}while (!WIFEXITED(status) && !WIFSIGNALED(status));

	}

};

int main(int argc, char const * argv[]) {

	FILE* fp;
	char* str;
	int g;
	str = "votetastroka!!!!!!";
	char str_in_file[buf];
	char str_in[buf] = "";
	g = strlen(str);

	if ((fp = fopen("./file.txt", "r")) == NULL) {
		printf("Не удается открыть файл.\n");
		exit(1);
	}
	while (fgets(str_in_file, sizeof(str_in_file), fp)) {

		strncpy(str_in, str_in_file, g);

		read_file(str, str_in);

	}

	if (fclose(fp)) {
		printf("Ошибка при закрытии файла.\n");
		exit(1);
	}
	
	return 0;
}