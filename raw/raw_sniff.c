#include <unistd.h> 
#include <sys/types.h>
#include <netinet/tcp.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <stdio.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <netinet/tcp.h> 
#include <netinet/ip.h>  
#include <getopt.h>
#include <mcheck.h>
#include <arpa/inet.h>
#include <net/ethernet.h>

#define MAC1_WIFI 0x7C
#define MAC2_WIFI 0x67
#define MAC3_WIFI 0xa2
#define MAC4_WIFI 0x3f
#define MAC5_WIFI 0x2c
#define MAC6_WIFI 0x62

#define MAC1_ETH 0x64
#define MAC2_ETH 0x00
#define MAC3_ETH 0x6a
#define MAC4_ETH 0xff
#define MAC5_ETH 0x79
#define MAC6_ETH 0xd1


void sniff(char *name, int mode);
void data_handler(unsigned char *buffer, int data_size, char *port, int mode);

int main(int argc, char **argv) {
	mtrace();
	if ((argc != 3)){
		printf("Usage: ./raw_sniff -flag <argument>\n");
		exit(-1);
	}
	int mode=0;
	int opt = 0;
	char *interface, *port, *ip;
	interface = malloc(sizeof(char) * 10);
	port = malloc(sizeof(char) * 10);
	ip = malloc(sizeof(char) * 15);
	while ( (opt = getopt(argc, argv, "i:p:s:")) != -1) {
		switch (opt) {

			case 'i':
			mode=1;
			strcpy(interface, optarg);
			sniff(interface, mode);
			break;

			case 'p':
			mode=2;
			strcpy(port, optarg);
			sniff(port, mode);
			break;
			
			case 's':
			mode=3;
			strcpy(ip, optarg);
			sniff(ip, mode);
			break;

        default: /* '?' */
			fprintf(stderr, "Использование : %s [-i] или [-p номер порта] или [-s (sender)ip-адресс{ формат: ххх.ххх.ххх.ххх }]\n", argv[0]);
			exit(EXIT_FAILURE);

		}
	}
	free(ip);
	free(interface);
	free(port); 
	return 0;
}


void sniff(char *name, int mode){
	int sock_raw, addr_size, data_size, i;
	struct sockaddr addr_sender;
	struct in_addr in;
	unsigned char *buffer = malloc(16384); // 2KiB
	sock_raw = socket( AF_PACKET , SOCK_RAW , htons(ETH_P_ALL));
	if(sock_raw < 0){
		printf("Error\n");
	}
	while(sock_raw > 0){

		addr_size= sizeof(addr_sender);
		data_size = recvfrom(sock_raw, buffer, 16384, 0, &addr_sender, &addr_size);
		if(data_size < 0){
			printf("recvfrom error\n");
		}
		data_handler(buffer, data_size, name, mode);
	}



}

void data_handler(unsigned char *buffer, int data_size, char *name,  int mode){
	int tcpheaderlen;
	unsigned short ipheaderlen;			
	struct iphdr *ipheader=(struct iphdr *)(buffer + sizeof(struct ethhdr));
	ipheaderlen = ipheader->ihl*4;
	struct tcphdr *tcpheader=(struct tcphdr *)(buffer + ipheaderlen + sizeof(struct ethhdr));
	tcpheaderlen = tcpheader->doff*4;
	unsigned char *data_buffer;
	data_buffer = (buffer + ipheaderlen + sizeof(struct ethhdr) + tcpheaderlen);
	if(mode==2){
		int nport=atoi(name);
		if(ntohs(tcpheader->source) == nport){
			printf("====================== tcpheader ======================\n");
			
			printf("Source tcp-port       : %d\n", ntohs(tcpheader->source) );
			printf("Destination tcp-port  : %d\n", ntohs(tcpheader->dest) );
			printf("Sequence namber       : %d\n", ntohs(tcpheader->seq) );
			printf("Acknowledgment number : %d\n", ntohs(tcpheader->ack_seq) );
			printf("Window                : %d\n", ntohs(tcpheader->window) );
			printf("Checksum              : %d\n", ntohs(tcpheader->check) );
			printf("Urgent pointer        : %d\n", ntohs(tcpheader->urg_ptr) );
			printf("Data                  : \n %s " , data_buffer);
			printf("\n====================== end ======================\n");
			
		}
	}else if(mode ==1){
		struct ethhdr *eth = (struct ethhdr *)buffer;
		if(!(strncmp(name,"wifi",4 ))){
			
			if(eth->h_dest[0] == MAC1_WIFI && eth->h_dest[1] == MAC2_WIFI && eth->h_dest[2] == MAC3_WIFI && eth->h_dest[3] == MAC4_WIFI && eth->h_dest[4] == MAC5_WIFI && eth->h_dest[5] == MAC6_WIFI){
				printf("====================== etherheader ======================\n");
				printf("Protocol     : %u\n", (unsigned short)eth->h_proto);
				printf("Destination Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X \n", eth->h_dest[0] , eth->h_dest[1] , eth->h_dest[2] , eth->h_dest[3] , eth->h_dest[4] , eth->h_dest[5] );
				printf("Source Address      : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X \n", eth->h_source[0] , eth->h_source[1] , eth->h_source[2] , eth->h_source[3] , eth->h_source[4] , eth->h_source[5] );
				printf("\n====================== end ======================\n");
			}
		}else if(!(strncmp(name,"ether",5 ))){

			if(eth->h_dest[0] == MAC1_ETH && eth->h_dest[1] == MAC2_ETH && eth->h_dest[2] == MAC3_ETH && eth->h_dest[3] == MAC4_ETH && eth->h_dest[4] == MAC5_ETH && eth->h_dest[5] == MAC6_ETH){
				printf("====================== etherheader ======================\n");
				printf("Protocol     : %u\n", (unsigned short)eth->h_proto);
				printf("Destination Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X \n", eth->h_dest[0] , eth->h_dest[1] , eth->h_dest[2] , eth->h_dest[3] , eth->h_dest[4] , eth->h_dest[5] );
				printf("Source Address      : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X \n", eth->h_source[0] , eth->h_source[1] , eth->h_source[2] , eth->h_source[3] , eth->h_source[4] , eth->h_source[5] );
				printf("\n====================== end ======================\n");
			}
			
		}
	}else if(mode==3){

		struct sockaddr_in source,dest;
		source.sin_addr.s_addr = ipheader->saddr;
		dest.sin_addr.s_addr = ipheader->daddr;
		if(!(strcmp(name, inet_ntoa(source.sin_addr)))){
			printf("====================== ipheader ======================\n");
			printf("Version                 : %d\n", ipheader->version);
			printf("Internet Header Length  : %d\n", ipheader->ihl);
			printf("Type of Service         : %d\n", ipheader->tos);
			printf("Total length            : %d\n", ntohs(ipheader->tot_len));
			printf("Identification          : %d\n", ntohs(ipheader->id));
			printf("Time to Live            : %d\n", ipheader->ttl);
			printf("Protocol                : %d\n", ipheader->protocol);
			printf("Header checksum         : %d\n", ntohs(ipheader->check));
			printf("Destination Address     : %s\n", inet_ntoa(dest.sin_addr));
			printf("Source Address          : %s\n", inet_ntoa(source.sin_addr) );
			printf("\n====================== end ======================\n");
		}
		

	}

}
