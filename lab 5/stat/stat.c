#include <stdio.h>

extern int plus(int, int);
extern int minus(int, int);

int main(int argc, char const *argv[])
{
	int x1 = 34;
	int x2 = 45;
	int	otv_plus = 0;
	int otv_minus = 0;
	otv_plus = plus(x1, x2);
	otv_minus = minus(x1, x2);
	printf("%d + %d = %d\n", x1, x2, otv_plus);
	printf("%d - %d = %d\n", x1, x2, otv_minus);
	return 0;
}