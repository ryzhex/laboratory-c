#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>


int main(int argc, char const *argv[])
{
	void* library_handler;
	int (*fplus)(int,int);
	int (*fminus)(int,int);

	library_handler = dlopen("/home/alex/Study/lessons C/lab 5/dinam_pod_stat/libpl_mins.so", RTLD_LAZY);
	if (!library_handler) {
		fprintf(stderr, "dlopen() error: %s\n", dlerror());
		exit(1);
	};
	fplus = dlsym(library_handler, "plus");
	fminus = dlsym(library_handler, "minus");

	int x1 = 34;
	int x2 = 45;
	int	otv_plus = 0;
	int otv_minus = 0;

	otv_plus = fplus(x1, x2);
	otv_minus = fminus(x1, x2);

	dlclose(library_handler);

	printf("%d + %d = %d\n", x1, x2, otv_plus);
	printf("%d - %d = %d\n", x1, x2, otv_minus);
	return 0;
}